# SIC 10 Dataset

## Resumo

  A rede  de  colaboração  científica  do Instituto Federal do Norte de Minas Gerais (IFNMG) em seus 10 primeiros anos (de 2009 a 2010 e de 2012 a 2019). Os dados foram coletados dos artigos publicados nos seminários de inovação e de iniciação científica do IFNMG (SIC). Nesta rede, o histórico de interações de colaboração científica entre um par de autores de um trabalho publicado no SIC determina a relação de coautoria entre eles. 

## Abstract

  The scientific collaboration network of the Instituto Federal do Norte de Minas Gerais (IFNMG) in its first 10 years (from 2009 to 2010 and from 2012 to 2019). Data were collected from articles published in the IFNMG innovation and scientific initiation seminars (SIC). In this network, the history of scientific collaboration interactions between a pair of authors of a work published in SIC determines the co-authorship relationship between them.

## Seminal Work:

[Uma análise temporal da rede de colaboração científica do IFNMG: 10 anos de iniciação científica e orientação acadêmica](https://jcloud.net.br/url/RosaAbdiel2019SIC10anosXISimposioJanuaria/)

### Bibliographies and Citations

#### ABNT:
LEÃO, J. C.; CARDOSO, R. J. D. S.; SANTOS, A. B. DOS. Uma análise temporal da rede de colaboração científica do IFNMG: 10 anos de iniciação científica e orientação acadêmica. Anais dos Simpósios de Informática do IFNMG-Campus Januária, v. 11, p. 7, 2019. Disponível em: https://jcloud.net.br/url/RosaAbdiel2019SIC10anosXISimposioJanuaria/.

#### APA:
Leão, J. C., Cardoso, R. J. D. S., & Santos, A. B. Dos. (2019). Uma análise temporal da rede de colaboração científica do IFNMG: 10 anos de iniciação científica e orientação acadêmica. Anais Dos Simpósios de Informática Do IFNMG-Campus Januária, 11, 7. Retrieved from https://jcloud.net.br/url/RosaAbdiel2019SIC10anosXISimposioJanuaria/.

#### IEEE:
[1] J. C. Leão, R. J. D. S. Cardoso, and A. B. Dos Santos, “Uma análise temporal da rede de colaboração científica do IFNMG: 10 anos de iniciação científica e orientação acadêmica”, An. dos Simpósios Informática do IFNMG-Campus Januária, vol. 11, p. 7, 2019.

#### Nature:
1. Leão, J. C., Cardoso, R. J. D. S. & Santos, A. B. Dos. Uma análise temporal da rede de colaboração científica do IFNMG: 10 anos de iniciação científica e orientação acadêmica. An. dos Simpósios Informática do IFNMG-Campus Januária 11, 7 (2019).

#### Vancouver:
1. Leão JC, Cardoso RJDS, Santos AB Dos. Uma análise temporal da rede de colaboração científica do IFNMG: 10 anos de iniciação científica e orientação acadêmica. An dos Simpósios Informática do IFNMG-Campus Januária [Internet]. 2019;11:7. Available from: https://jcloud.net.br/url/RosaAbdiel2019SIC10anosXISimposioJanuaria/.

### Bibliographic information file
```bib
@Article{RosaAbdiel2019SIC10anosXISimposioJanuaria,
author  = "Jeancarlo Campos Leão and Rosa Jaqueline De Souza Cardoso and Abdiel Batista Dos Santos",
title="Uma an{\'{a}}lise temporal da rede de colabora{\c{c}}{\~{a}}o cient{\'{i}}fica do IFNMG: 10 anos de inicia{\c{c}}{\~{a}}o cient{\'{i}}fica e orienta{\c{c}}{\~{a}}o acad{\^{e}}mica",
keywords = {Iniciação Científica, Análise de Redes Sociais, Redes de Colaboração Científica},
instituition ={{Instituto Federal do Norte de Minas Gerais - {{IFNMG}}}},
address = {Janu{\'a}ria/MG},
pages   = "7",
year="2019",
month="Sep",
day="19",
journal={Anais dos Simp{\'o}sios de Inform{\'a}tica do {{IFNMG}} - Campus Janu{\'a}ria},
volume = "11",
issn={2447-3847},
url={https://jcloud.net.br/url/RosaAbdiel2019SIC10anosXISimposioJanuaria/},
proceedings={http://anais.simposioinformatica.ifnmg.edu.br/ojs/index.php/anaisviiisimposio/article/view/123/102}
}
```

## Other studies on these data

### 2020

[Overcoming Bias in Community Detection Evaluation](http://research.jcloud.net.br/bib/?q=LLD20JIDM)

[Qual a área delas? Uma análise da autoria feminina nas áreas temáticas dos Seminários de Iniciação Científica do IFNMG](http://research.jcloud.net.br/bib/?q=WiSci2020Amariles)

[Top 10: quem são elas na ciência?](http://research.jcloud.net.br/bib/?q=WiSci2020Top10)

[Análise das relações de colaboração com participação feminina no Seminário de Iniciação Científica do IFNMG](http://research.jcloud.net.br/bib/?q=WiSci2020RosaSIC)

### 2019

[Análise da Rede Social do Seminário de Iniciação Científica do IFNMG](http://research.jcloud.net.br/bib/?q=Leao19IFJTIIC)

[Uma análise temporal da rede de colaboração científica do IFNMG: 10 anos de iniciação científica e orientação acadêmica](http://research.jcloud.net.br/bib/?q=RosaAbdiel2019SIC10anosXISimposioJanuaria)